# Scraper

The scraper is run manually by the developer when quests need to be updated.

# Display

The display is the front-end representation of the quest trees. 

# Format of scraped quests

## Quest object example
```json
{
  "name": "Cabin Fever",
  "difficulty": "Experienced",
  "length": "Medium",
  "questPoints": 2,
  "membersOnly": true,
  "required": ["Rum Deal", "Pirate's Treasure"],
  "requiredBy": "Great Brain Robbery, The",
  "skills": [
      "5 Fletching",
      "30 Cooking",
      "30 Ranged"
    ],
  "items": {
    "start": "2 Ecto-tokens and Ghostspeak Amulet (If you have not completed the Ghosts Ahoy quest).",
    "complete": "None.",
    "recommended": "Food and Ring of charos (a).",
    "acquired": "5 Fuses (Cabin Fever), 4 Ropes, Tinderbox, 6 Repair planks, 30 Tacks, 3 Swamp paste, Hammer, 10 Plunders, Cannon barrel (Cabin Fever), 4 Canisters, Ramrod, 4 Gunpowders, and 3 Cannonballs."
  },
  "reward": "7K Smithing XP, 7K Crafting XP, 7K Agility XP, Book o' Piracy, 10K Coins, 50% price reduction when chartering boats, access to Mos Le'Harmless, and 2 Treasure Hunter keys."
}
```