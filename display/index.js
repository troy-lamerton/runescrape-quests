// display the quest map
// this is the file which will be bundled

const tableify = (tableData) => {
  const table = document.createElement('table');
  const tableBody = document.createElement('tbody');

  tableData.forEach(function(rowData) {
    const row = document.createElement('tr');

    rowData.forEach(function(cellData) {
      const cell = document.createElement('td');
      const tableLabelClass = 'table-label'
      if (typeof cellData === 'object' && cellData.classList.contains(tableLabelClass)) {
        cell.classList = tableLabelClass
      }
      cell.appendChild(cellData);
      row.appendChild(cell);
    });

    tableBody.appendChild(row);
  });

  table.appendChild(tableBody);
  return table;
}

const questTrees = require('../data/questTrees.json').trees
const quests = require('../data/quests.json')

$(document).ready(function() {
  // process questTrees.json into jstrees accepted format

  function formatQuest (quest) {

    quest.text = quest.name
    delete quest.name

    quest.state = {
      opened: true
    }

    let color = 'black'
    const targetQuestData = quests.find(questData => questData.name === quest.text)

    if (!targetQuestData) {
      console.error(quest + ' not found in db')
    }

    quest.a_attr = {
      class: targetQuestData.difficulty
    }

    quest.children = quest.children.map(childQuest => formatQuest(childQuest))

    return quest
  }

  function formatQuestTree (tree) {
    const formattedTree = formatQuest(tree.head)

    return {
      core: {
        data: [
          formattedTree
        ]
      }
    }
  }

  // add a jstree to the document for each quest tree in the data
  questTrees.forEach((tree, index) => {
    const questTreeDiv = document.createElement('div')
    questTreeDiv.classList = 'quest-tree'

    // create the jstree
    $(questTreeDiv).jstree(formatQuestTree(tree))

    // create the info box
    const infoBox = document.createElement('div')
    infoBox.id = `info-box-${index + 1}`
    infoBox.classList = 'quest-info'
    // display prompt inside info box
    const prompt = document.createElement('p')
    prompt.classList = 'prompt'
    prompt.textContent = 'Click on a quest to see more info'
    $(infoBox).append(prompt)

    // append the tree and info box to the tree list
    const section = document.createElement('section')

    // create inner div for quest information
    const questInfoContainer = document.createElement('div')
    questInfoContainer.id = `inner-${infoBox.id}`
    $(infoBox).append(questInfoContainer)

    $(section).append(questTreeDiv)
      .append(infoBox)

    $('.wrapper').append(section)

  })

  // display quest info when a quest is clicked */
  $('.quest-tree').on('activate_node.jstree', (e, data) => {
    // retrieve quest info
    const targetQuest = quests.find(quest => quest.name === data.node.text)

    // create elements to display inside info box
    const title = document.createElement('h2')
    title.textContent = targetQuest.name

    // difficulty
    const difficultyHeader = document.createElement('span')
    difficultyHeader.textContent = 'Difficulty:'
    difficultyHeader.classList = 'table-label'

    const difficulty = document.createElement('p')
    difficulty.textContent = targetQuest.difficulty
    difficulty.classList = targetQuest.difficulty + ' jstree-anchor'
    const difficultyColorDot = document.createElement('i')
    difficultyColorDot.classList = 'jstree-icon jstree-themeicon'
    $(difficulty).prepend(difficultyColorDot)


    // quest length
    const questLengthHeader = document.createElement('span')
    questLengthHeader.classList = 'table-label'
    questLengthHeader.textContent = 'Length:'
    const questLength = document.createElement('p')
    questLength.textContent = targetQuest.length

    const skillsAndOtherHeader = document.createElement('span')
    skillsAndOtherHeader.textContent = 'Requirements:'
    skillsAndOtherHeader.classList = 'table-label'

    let skillsAndOther
    if (targetQuest.skills.length > 0) {
      skillsAndOther = document.createElement('ul')
      targetQuest.skills.forEach(skill => {
        $(skillsAndOther).append($(document.createElement('li')).text(skill))
      })
    } else {
      skillsAndOther = document.createElement('p')
      skillsAndOther.textContent = 'None.'
    }

    const rewardHeader = document.createElement('span')
    rewardHeader.textContent = 'Reward:'
    rewardHeader.classList = 'table-label'
    const reward = document.createElement('p')
    reward.textContent = targetQuest.reward
    
    const guideLinkLabel = document.createElement('span')
    guideLinkLabel.textContent = 'Quest guide:'
    guideLinkLabel.classList = 'table-label'
    
    const guideLink = document.createElement('a')
    guideLink.textContent = 'View guide...'
    let linkRoute = targetQuest.name
    // if the quest's name on RS Wiki would start with "A " or "The ",
    // rune hq's quest name needs to be changed to match
    let prefix = linkRoute.match(/[\w ]+,\ (The|A)/)
    if (prefix) {
      prefix = prefix[1]
      linkRoute = prefix + ' ' + linkRoute.slice(0, linkRoute.length - (prefix.length + 2))
    }
    guideLink.title = linkRoute + ' quest guide on RS Wiki'
    linkRoute = linkRoute.replace(/\s/g, '_')
    guideLink.href = 'http://runescape.wikia.com/wiki/' + linkRoute
    guideLink.target = '_blank'
    guideLink.rel = 'noopener'

    /* Next steps:
       * quest info: 
         [x] Add link to RS Wiki quest guide
         [ ] Add items needed to start and complete
       [ ] Ability to check off done quests, 
         [ ] remembered by cookie(s)
     */

    // dom elements in array format
    const questInfoTable = tableify([
      [difficultyHeader, difficulty],
      [questLengthHeader, questLength],
      [skillsAndOtherHeader, skillsAndOther],
      [guideLinkLabel, guideLink],
      [rewardHeader, reward]
    ])

    // select info box that is inside target quest's tree div
    const infoBoxIdNumber = data.event.target.id.match(/\d+/)
    const infoBoxIdValue = `info-box-${infoBoxIdNumber}`
    const infoBoxId = '#' + infoBoxIdValue
    // select inner div holding quest information
    const questInfoContainer = `#inner-${infoBoxIdValue}`
    // append quest info to container
    $(questInfoContainer).empty()
      .append(title)
      .append(questInfoTable)

    // remove the prompt text
    $(`${infoBoxId} > .prompt`).remove()

    $(infoBoxId)
      .css({
          alignItems: 'flex-start',
          flexDirection: 'column'
      })
  })

  /* when scrolling, the quest info box should 
  remain on the page until it can no longer fight inside its container
  and on the page at the same time */
  $(window).scroll(function() {
    const numQuestTrees = $('section').length 
    let containerId, bannerId
    for (let i = 1; i <= numQuestTrees; i++) {
      containerId = '#info-box-' + i
      bannerId = '#inner-info-box-' + i

      let questInfoContainerTop = $(containerId).offset().top    
      
      let sTop = $(window).scrollTop()
      let containerBottom = questInfoContainerTop + $(containerId).height()
      

      /* if user has scrolled past the top of the quest info container
      quest info box will scroll with the page, moving inside
      the quest info container */
      if (sTop > questInfoContainerTop && sTop < containerBottom) {
        // if banner cannot fit on screen inside infobox
        if (containerBottom-sTop < $(bannerId).height()) {
          // leave it at the end of the infobox
          $(containerId).addClass('justify-end')
          $(bannerId).removeClass('fixed')
        } else {
          // scroll with the page
          $(bannerId).addClass('fixed')
          $(containerId).removeClass('justify-end')
        }
      } else {
        // return quest info box to default position
        $(bannerId).removeClass('fixed')
        $(containerId).removeClass('justify-end')
      }
    }
  })
  
})

