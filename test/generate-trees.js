const test = require('tape')

const generator = require('../lib/generate-trees')

test('tree generator', function (t) {
  const result = generator(quests)

  t.ok(result.standAlone instanceof Array, 'result includes an array of stand-alone quests')
  t.ok(result.trees instanceof Array, 'result includes an array of tree')

  t.equal(result.standAlone.length, 1, 'one quest is a stand-alone quest')
  t.equal(result.trees.length, 1, 'there should be one quest tree')

  t.equal(result.trees[0].head.name, 'Advanced Tactics', 'Advanced Tactics should be the head of the quest tree')
  t.deepEqual(result.trees[0], questTree, 'the resultant quest tree should be deeply equal to the expected quest tree')

  t.end()
})

const questTree = {
  head: {
    name: "Advanced Tactics",
    children: [
      {
        name: "Second Intro",
        children: [
          {
            name: "Intro",
            children: []
          }
        ]
      },
      {
        name: "Extra Tips",
        children: [
          {
            name: "Intro",
            children: []
          }
        ]
      }
    ]
  }
}

const quests = [
  {
    "name": "Standing Lone",
    "required": [],
    "requiredBy": [],
  },
  {
    "name": "Intro",
    "required": [],
    "requiredBy": [
      "Second Intro"
    ],
  },
  {
    "name": "Second Intro",
    "required": [
      "Intro"
    ],
    "requiredBy": [
      "Advanced Tactics"
    ],
  },
  {
    "name": "Extra Tips",
    "required": [
      "Intro"
    ],
    "requiredBy": [
      "Advanced Tactics"
    ],
  },
  {
    "name": "Advanced Tactics",
    "required": [
      "Second Intro",
      "Extra Tips"
    ],
    "requiredBy": [],
  }
]
