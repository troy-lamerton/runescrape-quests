const fs = require('fs')
const path = require('path')

const generateTrees = require('./generate-trees')
// const quests = require('../data/quests.json')

generate(path.join(__dirname, '../data/questTrees.json'), generateTrees)

function generate (outputPath, treeGenerator) {

  function saveTreesToFile (data) {
    fs.writeFile(outputPath, JSON.stringify(data), (err) => {
      if (err) {
        console.log('Error while writing quest trees data to file:', outputPath)
        throw err;
      }
      console.log('Quest trees saved to:', outputPath)
      console.log('Total execution time:', Date.now() - startTime)
    })
  }

  const startTime = Date.now()

  fs.readFile(path.join(__dirname, '../data/quests.json'), 'utf8', function (err, data) {
    if (err) throw err; // we'll not consider error handling for now
    quests = JSON.parse(data);

    const treesData = treeGenerator(quests)
    saveTreesToFile(treesData)
  });
}

module.exports = generate