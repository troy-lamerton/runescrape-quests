// Function for scraping quests from the page at http://www.runehq.com/quest
// This function returns an array of objects following the format defined in the readme

const cheerio = require('cheerio')
const cheerioTableparser = require('cheerio-tableparser')
const MatrixTransposer = require('matrix-transposer').default;
const request = require('request')
const ProgressBar = require('progress')

const scrapeRunehqQuest = require('./runehq-quest-details')
const SITE_URL = 'http://www.runehq.com'

function scrapeRunehq (html, callback) {
  let $ = cheerio.load(html)
  cheerioTableparser($);

  const data = $('table').parsetable()

  // transpose data so that result is array of quest rows
  let quests = MatrixTransposer.arrayTranspose(data);

  // remove the table headers
  quests = quests.slice(2) // to scrape only a few quests, .slice(2, <a number a bit larger than 2 here>)
  const questsDetailed = []

  // format the quests array into a readable array of objects
  // and populate it with more details from each quest's page
  let itemsProcessed = 0
  const progressBar = new ProgressBar('Scraping quests [:bar] :percent', {
    complete: '▇',
    incomplete: '.',
    total: quests.length
  })
  quests.forEach((quest, index) => {

    // request quest page and scrape details
    const questRoute = $(quest[0]).attr('href') || $('a', quest[0]).attr('href')
    if (questRoute) {
      request(SITE_URL + questRoute, (err, res, html) => {
        if (!err && res.statusCode === 200) {
          if (html) {
            const name = $('a', quest[0]).text()
            const qDetails = scrapeRunehqQuest(html, name)
            
            // format basic quest info and append detailed info

            questsDetailed.push(
              Object.assign({
                name,
                difficulty: quest[1],
                length: quest[2],
                questPoints: parseInt(quest[3], 10),
                membersOnly: (quest[4] === 'Members') ? true : false,
              },
              qDetails)
            )
          }
          itemsProcessed++
          progressBar.tick()
          if(itemsProcessed === quests.length) {
            callback(questsDetailed)
          }
        } else {
          itemsProcessed++
          console.error('Request error:', err)
        }
      })
    } else {
      console.error(`Quest route ${questRoute} is invalid`)
    }

  })
}

exports.scrape = scrapeRunehq
exports.url = SITE_URL + '/quest'