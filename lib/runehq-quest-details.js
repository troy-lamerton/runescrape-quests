/* Function for scraping the details of a quest on RuneHQ, including other quests required and rewards. */

const cheerio = require('cheerio')

function scrapeRunehqQuest (html, questName) {
  let $ = cheerio.load(html)

  const questDetails = {
    required: [],
    requiredBy: [],
    skills: [],
    items: {
      start: '',
      complete: ''
      /*recommended: '',
      acquired: ''*/
    },
    reward: ''
  }

  const contentContainer = $('.content-container', '.main-content').eq(1)
  const questHtml = $('.content-body', contentContainer).children()

  /* There are miniquests that are required to enter certain areas containing quest NPC's.
   * It is therefore a requirement to complete these miniquests to start
   * some quests, but miniquests are not listed as quests themselves.
   * This codeblock removes the Bar Crawl and other miniquests from quests required.
   * To prevent complications when required quests are later mapped into trees.
   */
  const miniquests = [
    'The Alfred Grimhand Bar Crawl',
    'Mage of Zamorak',
    'The Hunt for Surok',
    'Barbarian Training'
  ]

  /* Quest Requirements */
  let qsRequired = questHtml.eq(5).text().split('\n')
  if (notNone(qsRequired)) {
    qsRequired = qsRequired.filter(name => {
      return miniquests.every(miniquestName => miniquestName !== name)
    })

    questDetails.required = qsRequired
  }
  
  let indexOffset = 0

  /* Skill/Other Requirements */
  const hasSkillReqs = questHtml.eq(6).text().startsWith('Skill/Other')
  if (hasSkillReqs) {
    let skills = questHtml.eq(7).text().split('\r\n')
    indexOffset += 2
    if (notNone(skills)) {
      skills = skills.filter(skill => skill !== '')
      questDetails.skills = skills
    }
  }
  
  /* Quests Unlocked/Partially Unlocked */
  const isRequiredBy = questHtml.eq(6 + indexOffset).text().startsWith('Quests Unlocked')
  
  if (isRequiredBy) {
    // get the text of each quest link
    let requiredBy = $('a', questHtml.eq(7 + indexOffset)).map(function(i, el) {
      return $(this).text();
    }).get()

    requiredBy = requiredBy.filter(name => {
      return miniquests.every(miniquestName => miniquestName !== name)
    })

    questDetails.requiredBy = requiredBy

    indexOffset += 2
  } else if (questName === 'Big Chompy Bird Hunting') {
  // manually correct requiredBy for a quest that is wrong on the rune hq site 
    let requiredBy = [
      'Mourning\'s Ends Part I',
      'Recipe for Disaster',
      'Zogre Flesh Eaters'
    ]
    questDetails.requiredBy = requiredBy
  }

  /* Items */
  const currentOffset = indexOffset
  // starting from first possible occurence of an Items section, 
  // up to last possible occurence of an Items section
  for (let i = 6 + currentOffset; i <= 12 + currentOffset; i+=2) {
    const sectionLabel = questHtml.eq(i).text()
    const isItemsSection = sectionLabel.startsWith('Items')
    // check if text starts with Items
    if (isItemsSection) {
      if (sectionLabel.match(/Start/)) {
        questDetails.items.start = questHtml.eq(i+1).text()
      } else if (sectionLabel.match(/Complete/)) {
        questDetails.items.complete = questHtml.eq(i+1).text()
      }
      // for each Items ... section found, add 2 to the indexOffset
      indexOffset += 2
    } else {
      break
    }
  }

  /* Reward */
  const reward = questHtml.eq(9 + indexOffset).text()
  questDetails.reward = reward.replace(/\r\n/g, '')

  return questDetails
}

function notNone (array) {
  if (array instanceof Array === false) return false
  if (typeof array[0] !== 'string') return false
  return !array[0].startsWith('None')
}

module.exports = scrapeRunehqQuest