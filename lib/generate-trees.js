// run this once to generate the tree structure
// this script takes the scraped quest data and restructures it to be more like a tree

function generator (quests) {

  function findChildren (name, quests) {
    const quest = quests.find(q => {
      return q.name === name
    })

    if (!quest) {
      throw `Error: Unable to find child quest named "${name}" in array of ${quests.length} non-standalone quests.`
    }

    let children = []
    
    if (quest.required.length > 0) {
      children = quest.required.map(qName => {
        return findChildren(qName, quests)
      })
    }

    return {
      name,
      children
    }
  }

  // standAlone is an array of stand-alone quests that are not required by other quests and have no quests required 
  const standAlone = []

  // heads is an array of quests that are not required by other quests, but have quests required
  const heads = []

  quests = quests.filter(q => {
    if (q.requiredBy.length === 0) {
      if (q.required.length === 0) {
        // filter out stand-alone quests
        standAlone.push(q)
        return false
      } else {
        // populate array of quest tree heads
        heads.push(q)
        return true
      }
    } else {
      return true
    }
  })

  /* generate quest trees
   *
   * start with the head of a tree, the final quest,
   * recursively add the children to the tree
   * until the whole tree is created 
   */

  const trees = heads.map(q => {
    return {
      head: {
        name: q.name,
        children: q.required.map(name => {
          return findChildren(name, quests)
        })
      }
    }
  })

  return {
    standAlone,
    trees
  }
}

module.exports = generator