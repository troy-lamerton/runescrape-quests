const request = require('request')
const fs = require('fs')
const path = require('path')

const scrapeRunehq = require('./runehq-quests')

scrapePage(scrapeRunehq.url, path.join(__dirname, '../data/quests.json'), scrapeRunehq.scrape)

function scrapePage (url, outputPath, scrapeFunction) {
  const startTime = Date.now()

  function saveToFile (quests) {
    console.log('saving data to:', outputPath)
    fs.writeFile(outputPath, JSON.stringify(quests), (err) => {
      if (err) {
        console.log('Error while writing quest data to file:', outputPath)
        throw err;
      }
      console.log('Execution time:', Date.now() - startTime)
      console.log('Quest data saved')
    })
  }

  request(url, (err, res, html) => {
    if (!err && res.statusCode === 200) {
      if (html) {
        const quests = scrapeFunction(html, saveToFile)

      } else {
        console.error('HTML error:', `The response from ${url} didn\'t return any html`)
      }
    } else {
      console.error('Request error:', err)
    }

  })

}


module.exports = scrapePage